@todo: add configurable setting to allow Masonry setting, and quick content link on top

Description
-----------
This module adds Masonry for Panels style.

Why Masonry?
When you have multiple arbitrary size block contents, Masonry optimizes layout of these contents within a display area of a specific width. The program acts like a good tetris player. By proper implementation, you can effectively maximize the number of arbitrary size contents that can be shown within the specific display area.

It is useful in these circumstances:

    If each content block size change dynamically
    Display area is small, or with arbitrary resolution. (e.g. display in mobile devices), and you want to maximize the space used for display, dynamically.
    You do not want (or lazy) to fix the layout in the panel.

For more information and examples about Masonry, or Jquery Masonry, please visit: http://masonry.desandro.com/
This module uses Vanilla Masonry (http://vanilla-masonry.desandro.com/ ) rather than Jquery Masonry for better operation.

To use it properly, after installing the module and change the Panel style of a Panel region to �gMasonry�h, you will also need to specify the width of each content in the Panel region. To do this, simply add a css class name in the �gCSS properties�h of the content, and specify the width of the css class somewhere (you can use �gcss injector�h module).
For example, if the class name under CSS properties of two contents are �gacontent�h and �gbcontent�h, add a CSS rule in css injector:
.acontent { width: 200px; }
.bcontent { width: 300px; }
You can use different width for different content.


Dependencies
------------
* Panels 3 (http://drupal.org/project/panels)
* CTools
Optional:
* CSS Injector

Installation
------------
1) Place this module directory in your modules folder (this will usually be
"sites/all/modules/").

2) Enable the module.

3) Go to the "Layout settings" tab of the Panels page, Mini panel, ... on
which you want to apply the "Masonry" style.

4) For each content type, add css class by 

5) Add at least a width to the content class. You can use "CSS injector" module:
5a) Install and enable CSS injector.
5b)  

Troubleshooting
---------------
This module has not been tested with multiple masonry per Panel, and will not expect to work out of the box. I am not sure the applicability of multiple masonry per panel, but please contribute patches if you require such functionality and has a solution.

Author
------
Teckilah

* mail: work@wimleers.com
* website: http://www.technologycomic.com
