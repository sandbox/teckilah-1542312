<?php
/**
 * @file
 * Definition of the 'Masonry' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Masonry'),
  'description' => t('Show panel panes in a region as Masonry.'),
  'render region' => 'panels_masonry_style_render_region',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_masonry_style_render_region($vars) {
  $display = $vars['display'];
  $region_id = $vars['region_id'];
  $owner_id = $vars['owner_id'];
  $panes = $vars['panes'];

  $masonry_id = 'panels-masonry-' . $owner_id . '-' . $region_id;
  $masonry_item_class = 'panels-masonry-container';

  $element = array(
    '#prefix' => '<div id="' . $masonry_item_class . '" id="' . $masonry_id . '">',
    '#suffix' => '</div>',
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'panels_masonry') . '/js/masonry.min.js' => array('type' => 'file', 'scope' => 'footer', 'weight' => 1),
        'jQuery(document).ready(function () { var wallPanelMasonry = new Masonry( document.getElementById("'.$masonry_item_class.'"), { columnWidth: 50 }); });' => array('type' => 'inline', 'scope' => 'footer', 'weight' => 2),
      ),
    ),
  );

  // Get the pane titles.
  // @todo: Add optional quick link
  /*$items = array();
  $delta = 1;
  foreach ($display->panels[$region_id] as $pane_id) {
    // Make sure the pane exists.
    if (!empty($panes[$pane_id])) {
      $title = panels_masonry_pane_titles($display->did, $pane_id);
      $title = $title ? $title : t('Item @delta', array('@delta' => $delta));
      $items[] = '<a href="#'. $masonry_id . '-' . $delta .'">' . $title . '</a>';
      ++$delta;
    }
  }

  $element['Masonry_title'] = array('#theme' => 'item_list', '#items' => $items);
  */
  $masonry_item_class = 'panels-masonry-item';
  $items = array();
  $delta = 1;
  foreach ($panes as $pane_id => $item) {
    $element['Masonry_content'][$pane_id] = array(
      '#prefix' => '<div class="' . $masonry_item_class . '" id="' . $masonry_id . '-' . $delta . '">',
      '#suffix' => '</div>',
      '#markup' => $item,
    );
    ++$delta;
  }

  return drupal_render($element);
  
}
